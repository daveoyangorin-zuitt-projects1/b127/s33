const mongoose = require('mongoose');
const courseSchema = new mongoose.courseSchema({
	name:{
		type: String,
		require:[true,"course is required"]
	},description:{
		type:String,
		required:[true,'description is required']
	},
	price:{
		type:Number,
		required:[true,'Price is required']
	},
	isActive:{
		type:Boolean,
		default:
	},
	createdOn:{
		type: Date,
		default:new Date()
	}
	//we will be applying the concept of referencing data to establish a relationship beetwen our courses and user
	enrollees:[
	{
		userId:{
			type: String,
			require:[true,'userId is required']
		},
		enrolledOn:{
			type:Date,
			default:new Date()
		}
	}
	]
});

module.exports = mongoose.model("course" , courseSchema);


const mongoose = require('mongoose');
const courseSchema = new mongoose.courseSchema({
	firstName:{
		type: String,
		require:[true,"firstName is required"]
	},lastName:{
		type:String,
		required:[true,'lastName is required']
	},
	email:{
		type:String,
		required:[true,'email is required']
	},
	password:{
		type:String,
		required:[true,'password is required']
	}
	isAdmin:{
		type:String,
		default:
	},
	mobileNo:{
		type: String,
		required:[true,'mobileNo is required']
	},
	enrollments:[
	{
		CourseId:{
			type: String,
			default:
		},
		enrolledOn:{
			type:Date,
			default:new Date()
		},
		status:{
			type:String,
			 default:enrolled()
		}
	}
	]
});

module.exports = mongoose.model("User" , courseSchema);