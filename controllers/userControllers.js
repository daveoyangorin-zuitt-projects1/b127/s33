const User = require('../models/User');
const bcrypt = require('bcryptjs');
const auth = require('../auth');

//Check if the email already exist
/*
steps:
1.Use mongoose "find" method to find duplicate emails
2.use the "then" method to send a response back to the client base on the result of the find method(email already exist/not)
*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find( {email: reqBody.email} ).then(result =>{
		if(result.length > 0){
			return true;
		}else{
			//no duplicate email found
			//user is not yet registered in the database
			return false;
		}
	})
}

//User registration
/*
1.create a new user object sing the mongoose model and the information from the request body.
2.error handling if error,return error.else , save the new user to the database
*/

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password,10)
	})

	//save the created object to our database
	return newUser.save().then((user , error) => {
		//User registration failed
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

//user authentication
/*
steps
1.Check the database if the user email exists
2.Compare the password provided in the login form with the password stored in the database
3.Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
	return User.findOne( {email: reqBody.email} ).then(result => {
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password)
			if(isPasswordCorrect){
				return { accessToken: auth.createAccessToken(result.toObject())}
			}else{
				//password do not match
				return false;
			}
		}
	})
}

//Find the document in the database using the user's ID

module.exports.getProfile = (data) => {
	return User.findById( {_id: data.id} ).then(result  => { 
		result.password = "";
		return result;
	})
}


