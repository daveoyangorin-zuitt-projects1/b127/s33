const jwt = require('jsonwebtoken');
//User defined string data that will be used to create our JSON web tokens
//Used in the algorithm for encrypting our data which makes it difficult to decode  the information without the defined secret keyword
const secret = 'CourseBookingAPI';

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email:user.email,
		isAdmin: user.isAdmin
	};
		return jwt.sign(data, secret,{})
}